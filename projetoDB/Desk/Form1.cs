﻿using Modelo.DAO;
using Modelo.PN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desk
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void BtnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnFazerCadastro_Click(object sender, EventArgs e)
        {
            Usuario p = new Usuario();

            p.Email = txtEmail.Text;
            p.Nome = txtNome.Text;

            if (!pnProjeto.Inserir(p))
            {
                MessageBox.Show("Algo deu errado!");
            }

            else
                MessageBox.Show("Cadastro realizado com sucesso!");
        }

        private void BtnBuscarEmail_Click(object sender, EventArgs e)
        {
            Usuario p = new Usuario();

            p = pnProjeto.Pesquisar(txtEmail.Text);

            if (p != null)
            {
                txtEmail.Text = p.Email;
                txtNome.Text = p.Nome;
            }
            else
            {
                MessageBox.Show("Este email não existe!");
            }
        }

        private void BtnExcluirCadastro_Click(object sender, EventArgs e)
        {
            Usuario p = new Usuario();

            p.Email = txtEmail.Text;
            p.Nome = txtNome.Text;

            if(MessageBox.Show("Confirma a exclusão?", "Atenção", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                pnProjeto.Excluir(p);
            }
        }

        private void BtnAlterarCadastro_Click(object sender, EventArgs e)
        {
            Usuario p = new Usuario();

            p.Email = txtEmail.Text;
            p.Nome = txtNome.Text;

            if (!pnProjeto.Alterar(p))
            {
                MessageBox.Show("Algo deu errado!");
            }

            else
                MessageBox.Show("Cadastro alterado com sucesso!");
        }

        private void BtnEnviarImagem_Click(object sender, EventArgs e)
        {
            Imagem i = new Imagem();

            i.Email = pnProjeto.Pesquisar(txtEmail.Text).Email;

            OpenFileDialog dialogo = new OpenFileDialog();

            dialogo.Title = "Selecione sua imagem";

            dialogo.InitialDirectory = @"C:\Downloads\Imagem";

            dialogo.Filter = "Arquivos texto (*.txt)|*.txt|Todos os arquivos (*.*)|*.*";

            DialogResult resposta = dialogo.ShowDialog();

            string caminhoCompleto = "";

            if (resposta == DialogResult.OK)
            {
                caminhoCompleto = dialogo.FileName;
                string text = System.IO.File.ReadAllText(caminhoCompleto);
                MessageBox.Show("Sinal enviado com sucesso!");

            }

            else
                MessageBox.Show("Por favor selecione um sinal!");

            if (i.Email != null)
            {
                i.Altura = Convert.ToInt32(txtAlturaImg.Text);
                i.Largura = Convert.ToInt32(txtLarguraImg.Text);
               // i.Imagem1 = caminhoCompleto;
            }
        }

        private void BtnBuscarImagem_Click(object sender, EventArgs e)
        {
            
        }
    }
}
