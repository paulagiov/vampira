﻿CREATE TABLE [dbo].[Imagem]
(
	[Id_Imagem] INT NOT NULL PRIMARY KEY, 
    [Altura] INT NOT NULL, 
    [Largura] INT NOT NULL,  
    [Email] VARCHAR(50) NOT NULL, 
    [Imagem] TEXT NOT NULL, 
    CONSTRAINT [Imagem_Usuario] FOREIGN KEY ([Email]) REFERENCES [Usuario]([Email])
)
