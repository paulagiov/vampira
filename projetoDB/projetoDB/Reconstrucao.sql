﻿CREATE TABLE [dbo].[Reconstrucao]
(
	[Id_Imagem_Saida] INT NOT NULL PRIMARY KEY, 
    [Data_Hora_Inicio] DATETIME NOT NULL, 
    [Data_Hora_Fim] DATETIME NOT NULL, 
    [Numero_de_Iteracoes] INT NOT NULL 
)
