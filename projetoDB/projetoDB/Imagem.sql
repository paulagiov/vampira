﻿CREATE TABLE [dbo].[Imagem]
(
	[Id_Imagem_Entrada] INT NOT NULL PRIMARY KEY autoinc(1),
	[Numero_Pixels_Coluna] INT NOT NULL,
	[Numero_Pixels_Linha] INT NOT NULL,
	[Email] VARCHAR(50) NOT NULL FOREIGN KEY ,
)

